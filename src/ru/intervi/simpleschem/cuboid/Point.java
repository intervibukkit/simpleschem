package ru.intervi.simpleschem.cuboid;

public class Point {
    public final double X, Y, Z;

    public Point(double x, double y, double z) {
        X = x;
        Y = y;
        Z = z;
    }

    @Override
    public String toString() {
        return "X=" + X + ", Y=" + Y + ", Z=" + Z;
    }
}
