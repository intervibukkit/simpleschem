package ru.intervi.simpleschem.cuboid;

import java.util.Iterator;

public class PointIterator implements Iterator<Point> {
    private final Cuboid CUBOID;
    private final double VOLUME;
    private double x, y, z, i = 0;

    public PointIterator(Cuboid cuboid) {
        CUBOID = cuboid;
        VOLUME = cuboid.getVolume();
        x = cuboid.MIN.X;
        y = cuboid.MIN.Y;
        z = cuboid.MIN.Z;
    }

    @Override
    public boolean hasNext() {
        return i < VOLUME;
    }

    @Override
    public Point next() {
        if (!hasNext()) return null;
        Point result = new Point(x, y, z);
        if (z == CUBOID.MAX.Z) {
            if (y == CUBOID.MAX.Y) {
                if (x < CUBOID.MAX.X) {
                    x++;
                    y = CUBOID.MIN.Y;
                    z = CUBOID.MIN.Z;
                }
            } else {
                y++;
                z = CUBOID.MIN.Z;
            }
        } else {
            z++;
        }
        i++;
        return result;
    }
}
