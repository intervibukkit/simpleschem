package ru.intervi.simpleschem.cuboid;

import java.util.Iterator;

public class Cuboid implements Iterable<Point> {
    public final Point FIRST, SECOND, MIN, MAX;

    public Cuboid(double x1, double y1, double z1, double x2, double y2, double z2) {
        FIRST = new Point(x1, y1, z1);
        SECOND = new Point(x2, y2, z2);
        double xMin, xMax, yMin, yMax, zMin, zMax;
        if (SECOND.X > FIRST.X) {
            xMin = FIRST.X;
            xMax = SECOND.X;
        } else {
            xMin = SECOND.X;
            xMax = FIRST.X;
        }
        if (SECOND.Y > FIRST.Y) {
            yMin = FIRST.Y;
            yMax = SECOND.Y;
        } else {
            yMin = SECOND.Y;
            yMax = FIRST.Y;
        }
        if (SECOND.Z > FIRST.Z) {
            zMin = FIRST.Z;
            zMax = SECOND.Z;
        } else {
            zMin = SECOND.Z;
            zMax = FIRST.Z;
        }
        MIN = new Point(xMin, yMin, zMin);
        MAX = new Point(xMax, yMax, zMax);
    }

    public Cuboid(Point first, Point second) {
        this(first.X, first.Y, first.Z, second.X, second.Y, second.Z);
    }

    public boolean contains(Point point) {
        return (point.X >= MIN.X && point.X <= MAX.X)
                && (point.Y >= MIN.Y && point.Y <= MAX.Y)
                && (point.Z >= MIN.Z && point.Z <= MAX.Z);
    }

    public double getWidth() {
        return MAX.X - MIN.X + 1;
    }

    public double getHeight() {
        return MAX.Z - MIN.Z + 1;
    }

    public double getDepth() {
        return MAX.Y - MIN.Y + 1;
    }

    public double getVolume() {
        return Math.abs(getWidth() * getHeight() * getDepth());
    }

    @Override
    public Iterator<Point> iterator() {
        return new PointIterator(this);
    }
}
