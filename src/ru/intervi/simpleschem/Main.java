package ru.intervi.simpleschem;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.plugin.java.JavaPlugin;
import ru.intervi.simpleschem.bukkit.BukkitRegionIterator;
import ru.intervi.simpleschem.bukkit.BukkitRegionPaste;
import ru.intervi.simpleschem.schema.SchemaReader;
import ru.intervi.simpleschem.schema.SchemaWriter;

import java.io.File;
import java.io.IOException;

public class Main extends JavaPlugin implements Listener {
    private static final File PATH = new File(
            new File(Main.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getParentFile(),
            "SimpleSchem"
    );
    private Location pos1, pos2;
    private boolean enable = false;

    @Override
    public void onEnable() {
        if (!PATH.isDirectory()) PATH.mkdirs();
        getServer().getPluginManager().registerEvents(this, this);
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (!enable) return;
        if (event.getHand() != EquipmentSlot.HAND) return;
        switch (event.getAction()) {
            case LEFT_CLICK_BLOCK:
                pos1 = event.getClickedBlock().getLocation();
                event.getPlayer().sendMessage("pos1 set");
                event.setCancelled(true);
                break;
            case RIGHT_CLICK_BLOCK:
                pos2 = event.getClickedBlock().getLocation();
                event.getPlayer().sendMessage("pos2 set");
                event.setCancelled(true);
                break;
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("simpleschem.admin")) {
            sender.sendMessage("no permission");
            return true;
        }
        if (args.length == 0) {
            sender.sendMessage("no args");
            return true;
        }
        switch (args[0].toUpperCase()) {
            case "P1":
                pos1 = ((Player) sender).getLocation().getBlock().getLocation();
                sender.sendMessage("pos1 set");
                break;
            case "P2":
                pos2 = ((Player) sender).getLocation().getBlock().getLocation();
                sender.sendMessage("pos2 set");
                break;
            case "SAVE":
                if (args.length < 2) {
                    sender.sendMessage("no name");
                    return true;
                }
                SchemaWriter writer = new SchemaWriter(new BukkitRegionIterator(
                        pos1.getWorld().getName(), pos1.getX(), pos1.getY(), pos1.getZ(),
                        pos2.getX(), pos2.getY(), pos2.getZ()
                ));
                try {
                    File file = new File(PATH, args[1] + ".ss");
                    if (file.isFile()) file.delete();
                    writer.write(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                sender.sendMessage(args[1] + " saved");
                break;
            case "PASTE":
                if (args.length < 3) {
                    sender.sendMessage("no name and empty");
                    return true;
                }
                BukkitRegionPaste paste = null;
                try {
                    paste = new BukkitRegionPaste(new SchemaReader(new File(PATH, args[1] + ".ss")));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                boolean empty = Boolean.parseBoolean(args[2]);
                try {
                    paste.paste(((Player) sender).getWorld(), empty);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                sender.sendMessage(args[1] + " pasted");
                break;
            case "TOGGLE":
                enable = !enable;
                sender.sendMessage("hand select: " + String.valueOf(enable));
                break;
            default:
                return false;
        }
        return true;
    }
}
