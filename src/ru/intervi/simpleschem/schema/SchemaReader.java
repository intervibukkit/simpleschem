package ru.intervi.simpleschem.schema;

import java.io.*;
import java.util.zip.GZIPInputStream;

public class SchemaReader {
    private final BufferedReader READER;

    public SchemaReader(File file) throws IOException {
        READER = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(file))));
    }

    public boolean hasNext() throws IOException {
        return READER.ready();
    }

    public String next() throws IOException {
        return READER.readLine();
    }

    public void close() throws IOException {
        READER.close();
    }
}
