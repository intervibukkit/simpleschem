package ru.intervi.simpleschem.schema;

import java.io.*;
import java.nio.file.FileAlreadyExistsException;
import java.util.Iterator;
import java.util.zip.Deflater;
import java.util.zip.GZIPOutputStream;

public class SchemaWriter {
    private final Iterator<String> ITER;

    public SchemaWriter(Iterator<String> iter) {
        ITER = iter;
    }

    public void write(File file) throws IOException {
        if (file.exists()) throw new FileAlreadyExistsException(file.getAbsolutePath() + " exists");
        file.createNewFile();
        BufferedWriter out = new BufferedWriter(
                new OutputStreamWriter(new GZIPOutputStream(new FileOutputStream(file)) {
            {
                def.setLevel(Deflater.BEST_COMPRESSION);
            }
        }));
        while (ITER.hasNext()) {
            out.write(ITER.next());
            out.newLine();
        }
        out.flush();
        out.close();
    }
}
