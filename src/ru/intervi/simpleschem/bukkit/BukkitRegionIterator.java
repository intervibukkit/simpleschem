package ru.intervi.simpleschem.bukkit;

import com.google.gson.Gson;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.material.*;
import org.bukkit.material.types.MushroomBlockTexture;
import ru.intervi.simpleschem.cuboid.Cuboid;
import ru.intervi.simpleschem.cuboid.Point;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class BukkitRegionIterator implements Iterator<String> {
    private final World WORLD;
    private final Iterator<Point> ITER;

    public BukkitRegionIterator(String worldName, double x1, double y1, double z1, double x2, double y2, double z2) {
        WORLD = Bukkit.getWorld(worldName);
        ITER = new Cuboid(x1, y1, z1, x2, y2, z2).iterator();
    }

    @Override
    public boolean hasNext() {
        return ITER.hasNext();
    }

    @Override
    @SuppressWarnings("deprecation")
    public String next() {
        if (!hasNext()) return null;
        Point point = ITER.next();
        Block block = WORLD.getBlockAt(new Location(WORLD, point.X, point.Y, point.Z));
        String data = new Gson().toJson(getDataMap(block.getState().getData()));
        HashMap<String, String> map = new HashMap<>();
        map.put("type", block.getType().toString());
        map.put("data", data);
        map.put("empty", String.valueOf(block.isEmpty()));
        map.put("x", String.valueOf(block.getX()));
        map.put("y", String.valueOf(block.getY()));
        map.put("z", String.valueOf(block.getZ()));
        map.put("meta", String.valueOf(block.getData()));
        return new Gson().toJson(map);
    }

    private Map<String, String> getDataMap(MaterialData data) {
        HashMap<String, String> map = new HashMap<>();
        if (data instanceof Coal) {
            CoalType type = ((Coal) data).getType();
            map.put("data", "COAL");
            map.put("type", type.toString());
        } else if (data instanceof Door) {
            Door door = ((Door) data);
            BlockFace face = door.getFacing();
            boolean hinge = door.getHinge();
            boolean open = door.isOpen();
            boolean half = door.isTopHalf();
            map.put("data", "DOOR");
            map.put("face", face.toString());
            map.put("hinge", String.valueOf(hinge));
            map.put("open", String.valueOf(open));
            map.put("half", String.valueOf(half));
        } else if (data instanceof LongGrass) {
            LongGrass grass = ((LongGrass) data);
            GrassSpecies species = grass.getSpecies();
            map.put("data", "GRASS");
            map.put("species", species.toString());
        } else if (data instanceof Mushroom) {
            Mushroom mushroom = ((Mushroom) data);
            MushroomBlockTexture texture = mushroom.getBlockTexture();
            map.put("data", "MUSHROOM");
            map.put("texture", texture.toString());
        } else if (data instanceof Rails) {
            Rails rails = ((Rails) data);
            BlockFace face = rails.getDirection();
            boolean slope = rails.isOnSlope();
            map.put("data", "RAILS");
            map.put("face", face.toString());
            map.put("slope", String.valueOf(slope));
        } else if (data instanceof Sandstone) {
            Sandstone stone = ((Sandstone) data);
            SandstoneType type = stone.getType();
            map.put("data", "SANDSTONE");
            map.put("type", type.toString());
        } else if (data instanceof Stairs) {
            Stairs stairs = ((Stairs) data);
            BlockFace face = stairs.getFacing();
            boolean inverted = stairs.isInverted();
            map.put("data", "STAIRS");
            map.put("face", face.toString());
            map.put("inverted", String.valueOf(inverted));
        } else if (data instanceof Wood) {
            Wood wood = ((Wood) data);
            TreeSpecies species = wood.getSpecies();
            map.put("data", "WOOD");
            map.put("species", species.toString());
        } else if (data instanceof Wool) {
            Wool wool = ((Wool) data);
            DyeColor color = wool.getColor();
            map.put("data", "WOOL");
            map.put("color", color.toString());
        } else if (data instanceof SmoothBrick) {
            SmoothBrick brick = ((SmoothBrick) data);
            String textures = new Gson().toJson(brick.getTextures());
            map.put("data", "BRICK");
            map.put("textures", textures);
        } else if (data instanceof TrapDoor) {
            TrapDoor door = ((TrapDoor) data);
            BlockFace face = door.getFacing();
            boolean open = door.isOpen();
            boolean inverted = door.isInverted();
            map.put("data", "TRAP");
            map.put("face", face.toString());
            map.put("open", String.valueOf(open));
            map.put("inverted", String.valueOf(inverted));
        } else if (data instanceof Directional) {
            map.put("data", "DIRECTIONAL");
            map.put("face", ((Directional) data).getFacing().toString());
        } else if (data instanceof Colorable) {
            map.put("data", "COLORABLE");
            map.put("color", ((Colorable) data).getColor().toString());
        } else if (data instanceof Openable) {
            map.put("data", "OPENABLE");
            map.put("open", String.valueOf(((Openable) data).isOpen()));
        }
        return map;
    }
}
