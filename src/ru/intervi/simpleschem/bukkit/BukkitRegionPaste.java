package ru.intervi.simpleschem.bukkit;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.material.*;
import org.bukkit.material.types.MushroomBlockTexture;
import ru.intervi.simpleschem.schema.SchemaReader;

import java.io.IOException;
import java.util.Map;

public class BukkitRegionPaste {
    private final SchemaReader READER;

    public BukkitRegionPaste(SchemaReader reader) {
        READER = reader;
    }

    @SuppressWarnings("deprecation")
    public void paste(World world, boolean empty) throws IOException {
        while (READER.hasNext()) {
            try {
                Gson gson = new Gson();
                Map<String, String> map = gson.fromJson(READER.next(), new TypeToken<Map<String, String>>(){}.getType());
                if (!empty && Boolean.parseBoolean(map.get("empty"))) continue;
                Material type = Material.valueOf(map.get("type"));
                int x = Integer.parseInt(map.get("x"));
                int y = Integer.parseInt(map.get("y"));
                int z = Integer.parseInt(map.get("z"));
                Location loc = new Location(world, x, y, z);
                Block block = loc.getBlock();
                block.setType(type);
                block.setData(Byte.valueOf(map.get("meta")));
                setState(
                        gson.fromJson(map.get("data"), new TypeToken<Map<String, String>>(){}.getType()),
                        block.getState().getData()
                );
                block.getState().update(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setState(Map<String, String> map, MaterialData data) {
        if (map.isEmpty()) return;
        if (data instanceof Coal) {
            ((Coal) data).setType(CoalType.valueOf(map.get("type")));
        } else if (data instanceof Door) {
            boolean hinge = Boolean.parseBoolean(map.get("hinge"));
            ((Door) data).setHinge(hinge);
        } else if (data instanceof LongGrass) {
            GrassSpecies species = GrassSpecies.valueOf(map.get("species"));
            ((LongGrass) data).setSpecies(species);
        } else if (data instanceof Mushroom) {
            MushroomBlockTexture texture = MushroomBlockTexture.valueOf(map.get("texture"));
            ((Mushroom) data).setBlockTexture(texture);
        } else if (data instanceof Stairs) {
            boolean inverted = Boolean.parseBoolean(map.get("inverted"));
            ((Stairs) data).setInverted(inverted);
        } else if (data instanceof Rails) {
            BlockFace face = BlockFace.valueOf(map.get("face"));
            boolean slope = Boolean.parseBoolean(map.get("slope"));
            ((Rails) data).setDirection(face, slope);
        } else if (data instanceof Sandstone) {
            SandstoneType type = SandstoneType.valueOf(map.get("type"));
            ((Sandstone) data).setType(type);
        } else if (data instanceof Wood) {
            TreeSpecies species = TreeSpecies.valueOf(map.get("species"));
            ((Wood) data).setSpecies(species);
        } else if (data instanceof TrapDoor) {
            BlockFace face = BlockFace.valueOf(map.get("face"));
            boolean inverted = Boolean.valueOf(map.get("inverted"));
            TrapDoor trap = ((TrapDoor) data);
            trap.setFacingDirection(face);
            trap.setInverted(inverted);
        }
        if (data instanceof Directional) {
            ((Directional) data).setFacingDirection(BlockFace.valueOf(map.get("face")));
        }
        if (data instanceof Colorable) {
            ((Colorable) data).setColor(DyeColor.valueOf(map.get("color")));
        }
        if (data instanceof Openable) {
            ((Openable) data).setOpen(Boolean.valueOf(map.get("open")));
        }
    }
}
